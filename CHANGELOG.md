## 0.0.3

* Add InputFormatter.

## 0.0.2

* Update licence.

## 0.0.1

* Initial release - Added ViewSwitcher, ConditionalView and LoadingView widgets.

import 'package:flutter/material.dart';

typedef ViewBuilderFunc = Widget Function();

class ViewSwitcher extends StatelessWidget {
  final ViewBuilderFunc view1;
  final ViewBuilderFunc view2;
  final bool showView1;

  const ViewSwitcher(
    this.showView1,
    this.view1,
    this.view2, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (showView1) {
      return view1();
    }
    return view2();
  }
}

class ConditionalView extends ViewSwitcher {
  ConditionalView(
    bool showView,
    ViewBuilderFunc view, {
    Key? key,
  }) : super(
          showView,
          view,
          () => const SizedBox.shrink(),
          key: key,
        );
}

class LoadingView extends ViewSwitcher {
  LoadingView(
    bool showView,
    ViewBuilderFunc view, {
    Key? key,
  }) : super(
          showView,
          view,
          () => const Center(
            child: CircularProgressIndicator(),
          ),
          key: key,
        );
}

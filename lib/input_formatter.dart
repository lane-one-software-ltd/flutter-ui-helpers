import 'package:flutter/services.dart';

enum InputFormatterType { upperCase, lowerCase, custom }

typedef CustomInputFormatter = String Function(String);

class InputFormatter extends TextInputFormatter {
  InputFormatterType inputFormatterType;
  late CustomInputFormatter _formatter;

  InputFormatter({required this.inputFormatterType, CustomInputFormatter? customFormatter}) {
    switch (inputFormatterType) {
      case InputFormatterType.upperCase:
        _formatter = _makeUpper;
        break;
      case InputFormatterType.lowerCase:
        _formatter = _makeLower;
        break;
      case InputFormatterType.custom:
        _formatter = customFormatter!;
        break;
    }
  }

  static toUpper() {
    return InputFormatter(
      inputFormatterType: InputFormatterType.upperCase,
    );
  }

  static toLower() {
    return InputFormatter(
      inputFormatterType: InputFormatterType.upperCase,
    );
  }

  static toCustom(CustomInputFormatter customInputFormatter) {
    return InputFormatter(
      inputFormatterType: InputFormatterType.custom,
      customFormatter: customInputFormatter,
    );
  }

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: _formatter(newValue.text),
      selection: newValue.selection,
    );
  }

  String _makeUpper(String value) {
    return value.toUpperCase();
  }

  String _makeLower(String value) {
    return value.toLowerCase();
  }
}

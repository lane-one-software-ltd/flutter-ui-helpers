The UI Helpers package provides small helper widgets to minimize the amount of code required for repetitive tasks.

## Features

The following widgets are provided:
* ViewSwitcher - Allows for switching between two views based on a boolean flag.
* ConditionalView - Conditionally shows a view based on a boolean flag.
* LoadingView - Shows a progress view or the actual view based on a boolean flag.

The following input formatters are provided:
* InputFormatter - Allows for easy formatting of text input (such as upper/lower case).

## Getting started

Import the package into your application

` flutter pub add laneonesoftware_ui_helpers `

## Usage

ViewSwitcher
```
  Widget build(BuildContext context) {
    var showHelloView = true;
    return ViewSwitcher(
        showHelloView,
        () => Text("Hello"),
        () => Text("World"),
    );
  }
```

InputFormatter
```
TextField(
    inputFormatters: [InputFormatter.toUpper()],
);
TextField(
    inputFormatters: [InputFormatter.toLower()],
);
TextField(
    inputFormatters: [InputFormatter.toCustom(_removeAllWhitespace)],
);

String _removeAllWhitespace(String value) {
  return value.replaceAll(' ', '');
}
```